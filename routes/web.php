<?php

use Illuminate\Routing\RouteGroup;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function () {

    Route::get('login', 'Admin\UserController@login');
    Route::post('login', 'Admin\UserController@auth');
    Route::any('logout', function() {
        Auth::logout();
        return redirect('/');
    });

    Route::get('/', function () {
        return redirect('admin/menu');
    })->middleware('role:admin|editor');
    
    // Route::get('/', function () {
    //     return 'Admin';
    // })->middleware(['role:admin']);

    Route::prefix('info-block')->middleware('role:admin|editor')->group(function () {
        Route::get('{archive?}', 'Admin\InfoBlockController@index');
        Route::post('ajax', 'Admin\InfoBlockController@ajax');
        Route::get('form/{id}', 'Admin\InfoBlockController@form');
        Route::post('save/{id}', 'Admin\InfoBlockController@save');
        Route::get('destroy/{id}', 'Admin\InfoBlockController@destroy');
        Route::get('restore/{id}', 'Admin\InfoBlockController@restore');
    }); 

    Route::prefix('menu/{subid?}')->middleware('role:admin|editor')->group(function () {
        Route::get('{archive?}', 'Admin\MenuController@index');
        Route::post('ajax', 'Admin\MenuController@ajax');
        Route::get('form/{id}', 'Admin\MenuController@form');
        Route::post('save/{id}', 'Admin\MenuController@save');
        Route::get('destroy/{id}', 'Admin\MenuController@destroy');
        Route::get('restore/{id}', 'Admin\MenuController@restore');
    });

    Route::prefix('group/{groupid}/articles')->middleware('role:admin|editor')->group(function () {
        Route::get('{archive?}', 'Admin\ArticleController@index');
        Route::post('ajax', 'Admin\ArticleController@ajax');
        Route::get('form/{id?}', 'Admin\ArticleController@form');
        Route::post('save/{id?}', 'Admin\ArticleController@save');
        Route::get('destroy/{id}', 'Admin\ArticleController@destroy');
        Route::get('restore/{id}', 'Admin\ArticleController@restore');
        Route::get('delete-img/{id}', 'Admin\ArticleController@deleteImg');
    });

    Route::prefix('preparation')->middleware('role:admin|editor')->group(function () {
        Route::get('{archive?}', 'Admin\PreparationController@index');
        Route::post('ajax', 'Admin\PreparationController@ajax');
        Route::get('form/{id?}', 'Admin\PreparationController@form');
        Route::post('save/{id?}', 'Admin\PreparationController@save');
        Route::get('destroy/{id}', 'Admin\PreparationController@destroy');
        Route::get('restore/{id}', 'Admin\PreparationController@restore');
        Route::get('delete-img/{id}', 'Admin\PreparationController@deleteImg');
    });

    Route::prefix('users/{role}')->middleware('role:admin|editor')->group(function () {
        Route::get('{archive?}', 'Admin\UserController@index');
        Route::post('ajax', 'Admin\UserController@ajax');
        Route::get('form/{id}', 'Admin\UserController@form');
        Route::post('save/{id}', 'Admin\UserController@save');
        Route::get('destroy/{id}', 'Admin\UserController@destroy');
        Route::get('restore/{id}', 'Admin\UserController@restore');
    });
});

Route::prefix(LaravelLocalization::setLocale())->middleware(['localizationRedirect', 'localeViewPath'])->group(function () {
    Route::get('/', 'Site\MainController@main');
    Route::get('{slug1}', 'Site\ArticleController@dispatcher');
    Route::get('{slug1}/{slug2}', 'Site\ArticleController@dispatcher');
    Route::get('{slug1}/{slug2}/{slug3}', 'Site\MainController@dispatcher');
    Route::post('sendfeedback', 'Site\MainController@sendfeedback');
    
    //Route::get('articles/{slug?}', 'Site\ArticleController@list');
    //Route::get('article/{slug}', 'Site\ArticleController@view');
    //Route::get('preparations/{slug}', 'Site\ArticleController@preparation');
    //Route::get('preparation/{slug}', 'Site\ArticleController@preparationView');
    
    
    //Route::get('news/{article}', 'Site\MainController@news');
    //Route::get('{group}/{article}', 'Site\MainController@articles');
    //Route::get('stroke-act-fast', 'Site\MainController@landing');
});
