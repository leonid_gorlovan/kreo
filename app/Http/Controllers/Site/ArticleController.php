<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use App\Menu;
use App\Preparation;
use SEOMeta;
use OpenGraph;
use Twitter;

class ArticleController extends Controller
{
    public function dispatcher($slug1 = null, $slug2 = null, $slug3 = null)
    {
        $menu = Menu::whereSlug($slug1)->orWhereTranslation('slug', $slug1)->first();

        if(object_get($menu, 'tmpl') == 'menu')
        {
            $menu = Menu::whereSlug($slug2)->orWhereTranslation('slug', $slug2)->first();
            $slug = $slug3;
        }
        else
        {
            $slug = $slug2;
        }

        $tmpl = object_get($menu, 'tmpl');

        if(!empty($slug) && $tmpl == 'preparations')
        {
            $tmpl = 'preparation';
        }
        elseif(!empty($slug) && $tmpl == 'articles')
        {
            $tmpl = 'article';
        }

        if(empty($menu)) abort('404');

        switch ($tmpl) {
            case 'articles':
                return $this->articleList($menu, $slug);
                break;

            case 'article':
                return $this->articleView($menu, $slug);
                break;

            case 'preparations':
                return $this->preparationList($menu, $slug);
                break;

            case 'preparation':
                return $this->preparationView($menu, $slug);
                break;

            case 'form':
                # code...
                break;
            
            default:
                # code...
                break;
        }
    }

    public function articleList($menu, $slug)
    {
        $articles = Article::whereGroupId($menu->id)->orderBy('created_at', 'desc')->paginate(5);

        SEOMeta::setTitle(object_get($menu, 'seo_title:' . $this->current_lang, object_get($menu, 'title:'  . $this->current_lang)));
        SEOMeta::setDescription(object_get($menu, 'seo_description:' . $this->current_lang));
        SEOMeta::setKeywords(object_get($menu, 'seo_keywords:' . $this->current_lang));

        return view('site.articles', compact('articles', 'menu'));
    }

    public function articleView($menu, $slug)
    {
        $article = $menu->content()->whereSlug($slug)->orWhereTranslation('slug', $slug)->first();
        // $preparation = Preparation::whereSlug($slug)->orWhereTranslation('slug', $slug)->first();

        if(empty($article)) abort('404');

        //$randomArticles = Article::whereGroupId($menu->id)->where('id', '!=', $article->id)->limit(3)->inRandomOrder()->get();
        $randomArticles = Article::where('id', '!=', object_get($article, 'id'))->limit(3)->inRandomOrder()->get();

        SEOMeta::setTitle(object_get($article, 'seo_title:' . $this->current_lang, object_get($article, 'title:'  . $this->current_lang)));
        SEOMeta::setDescription(object_get($article, 'seo_description:' . $this->current_lang));
        SEOMeta::setKeywords(object_get($article, 'seo_keywords:' . $this->current_lang));

        return view('site.article', compact('article', 'randomArticles'));
    }

    public function preparationList($menu, $slug)
    {
        // $preparations = Preparation::join('preparation_translations as t', function ($join) {
        //     $join->on('preparations.id', '=', 't.preparation_id')
        //         ->where('t.locale', '=', $this->current_lang);
        // }) 
        // ->orderBy('t.title', 'asc')
        // ->with('translations')
        // ->paginate(5);

        ///////

        $menu = \App\Menu::whereTmpl('preparations')->first();
        $page = request('page', 1);

        $link = \LaravelLocalization::getLocalizedURL($this->current_lang, object_get($menu, 'slug:' . $this->current_lang));

        if(request()->has('page')) $link = $link . '?page=' . $page;

        ///////

        $preparations = \DB::table('preparations')
        ->join('preparation_translations', 'preparations.id', '=', 'preparation_translations.preparation_id')
        ->select('*')
        ->where('preparation_translations.locale', '=', $this->current_lang)
        ->orderBy('preparation_translations.title', 'asc')
        ->paginate(5);

        SEOMeta::setTitle(object_get($menu, 'seo_title:' . $this->current_lang, object_get($menu, 'title:'  . $this->current_lang)));
        SEOMeta::setDescription(object_get($menu, 'seo_description:' . $this->current_lang));
        SEOMeta::setKeywords(object_get($menu, 'seo_keywords:' . $this->current_lang));

        return view('site.preparats', compact('preparations', 'link'));
    }

    public function preparationView($menu, $slug)
    {
        $preparation = Preparation::whereSlug($slug)->orWhereTranslation('slug', $slug)->first();
        if(empty($preparation)) abort('404');
        $randomArticles = Preparation::where('id', '!=', $preparation->id)->limit(3)->inRandomOrder()->get();

        SEOMeta::setTitle(object_get($preparation, 'seo_title:' . $this->current_lang, object_get($preparation, 'title:'  . $this->current_lang)));
        SEOMeta::setDescription(object_get($preparation, 'seo_description:' . $this->current_lang));
        SEOMeta::setKeywords(object_get($preparation, 'seo_keywords:' . $this->current_lang));

        return view('site.preparat', compact('preparation', 'randomArticles'));
    }
}
