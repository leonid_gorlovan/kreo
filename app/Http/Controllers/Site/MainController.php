<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SendFeedbackPost;
use App\Mail\FeedbackMail;
use App\Menu;
use App\InfoBlock;
use App\Preparation;

class MainController extends Controller
{
    public function main()
    {
        $menu = Menu::find(14);
        
        //dd( object_get($menu, 'revsub.slug:en') );
        //dd( object_get($menu, 'title:' . $this->current_lang) );

        $infoBlock = InfoBlock::orderBy('position')->limit(3)->get();
        $lastPreparation = Preparation::orderBy('created_at', 'desc')->limit(4)->get();
        return view('site.main', compact('infoBlock', 'lastPreparation'));
    }

    public function sendfeedback(SendFeedbackPost $request)
    {
        //\Mail::to(env('MAIL_TO'))->send(new FeedbackMail());

        //Сообщение с сайта

        $from_user = "=?UTF-8?B?".base64_encode('Повідомлення з сайту creopharm.com.ua')."?=";
        $subject = "=?UTF-8?B?".base64_encode(request('type') . ' | creopharm.com.ua')."?=";

        $headers = "From: creopharm <no-reply@creopharm.com.ua>\r\n".
               "MIME-Version: 1.0" . "\r\n" .
               "Content-type: text/html; charset=UTF-8" . "\r\n";

        $message = '<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Повідомлення з сайту creopharm.com.ua</title>
        </head>
        <body>
            <h1>Повідомлення з сайту creopharm.com.ua</h1>
            <p><b>Ім\'я:</b> ' . request('name') . '</p>
            <p><b>Email:</b> ' . request('email') .'</p>
            <p><b>Тема листа:</b> ' . request('type') . '</p>
            <p><b>Повідомлення:</b> ' . request('text')  . '</p>
        </body>
        </html>';

        mail(env('MAIL_TO'), $subject, $message, $headers); 

        return redirect()->back()->with(['message'=>true]);
    }
}
