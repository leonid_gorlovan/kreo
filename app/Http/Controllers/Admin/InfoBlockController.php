<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\InfoBlock;

class InfoBlockController extends Controller
{
    public function __construct()
    {
        //parent::__construct();
        \App::setLocale('ru');
    }

    public function ajax(Request $request)
    {
        $infoBlock = InfoBlock::query();

        if($request->input('isArchive'))
        {
            return datatables()->of($infoBlock->onlyTrashed()
            )->toJson();
        }
        else
        {
            return datatables()->of($infoBlock)->toJson();
        }
    }

    public function index($isArchive = false)
    {
        $isArchive = !empty($isArchive) ? 1:0;
        return view('admin.infoblock', compact('isArchive'));
    }

    public function form($id = 0)
    {
        $infoBlock = InfoBlock::find($id);
        $lastPosition = InfoBlock::lastPossition();
        return view('admin.infoblock_form', compact('infoBlock', 'lastPosition'));
    }

    public function save(Request $request, $id = 0)
    {
        $infoBlock = InfoBlock::saveData($request, $id);
        \Notify::success('Сохранено');
        return redirect('admin/info-block/form/' . object_get($infoBlock, 'id'));
    }

    public function destroy($id)
    {
        $infoBlock = InfoBlock::find($id);
        if(!empty($infoBlock)) $infoBlock->delete();
        \Notify::success('Отправлено в архив');
        return redirect('admin/info-block');
    }

    public function restore($id)
    {
        $infoBlock = InfoBlock::withTrashed()->find($id);

        if(!empty($infoBlock))
        {
            $infoBlock->restore();
            \Notify::success('Восстановлено');
        }

        return redirect('admin/info-block');
    }
}
