<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use App\Menu;

class ArticleController extends Controller
{
    public function __construct()
    {
        //parent::__construct();
        \App::setLocale('ru');
    }

    public function ajax(Request $request, $subid = 0)
    {
        $articles = Article::query()->with('group');

        if(!empty($subid))
        {
            $articles = $articles->whereGroupId($subid);
        }

        if($request->input('isArchive'))
        {
            return datatables()->of($articles->onlyTrashed()
            )->toJson();
        }
        else
        {
            return datatables()->of($articles)->toJson();
        }
    }

    public function index($group_id, $isArchive = false)
    {
        $isArchive = !empty($isArchive) ? 1:0;
        $menu = Menu::find($group_id);
        return view('admin.article', compact('isArchive', 'menu', 'group_id'));
    }

    public function form($group_id, $id = 0)
    {
        $article = Article::find($id);
        $menuTree = Menu::selectTree();
        $menu = Menu::find($group_id);
        return view('admin.article_form', compact('article', 'group_id', 'menu', 'menuTree'));
    }

    public function save(Request $request, $group_id, $id = 0)
    {
        $article = Article::saveData($request, $group_id, $id);
        \Notify::success('Сохранено');
        return redirect('admin/group/' . $group_id . '/articles/form/' . object_get($article, 'id'));
    }

    public function destroy($group_id, $id)
    {
        $article = Article::find($id);
        if(!empty($article)) $article->delete();
        \Notify::success('Отправлено в архив');
        return redirect('admin/group/' . $group_id . '/articles');
    }

    public function restore($group_id, $id)
    {
        $data = Article::withTrashed()->find($id);

        if(!empty($data))
        {
            $data->restore();
            \Notify::success('Восстановлено');
        }

        return redirect('/admin/group/' . $group_id . '/articles');
    }

    public function deleteImg($group_id, $id)
    {
        $data = Article::find($id);

        if(!empty($data))
        {
            $path = public_path('uploads/articles');
            @unlink($path . '/' . $data->image);

            $data->image = null;
            $data->save();

            \Notify::success('Изображение Удалено');
        }

        return redirect('admin/group/' . $group_id . '/articles/form/' . $id);
    }
}
