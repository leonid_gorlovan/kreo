<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserPostRequest;
use App\User;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        //parent::__construct();
        \App::setLocale('ru');
    }

    public function login()
    {
        // $user = new User;
        // $user->name = 'Leoni Gor';
        // $user->email = 'leoni@ukr.net';
        // $user->password = \Hash::make('bny76bh67');
        // $user->save();
        // $user->syncRoles(['admin']);

        return view('vendor.adminlte.login');
    }

    public function auth(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials))
        {
            return redirect()->intended('admin');
        }

        \Notify::warning('Не удалось авторизоваться');

        return redirect('admin/login');
    }

    public function ajax(Request $request, $role = 0)
    {
        if($request->input('isArchive'))
        {
            return datatables()->of(User::query()->role($role)->onlyTrashed()
            )->toJson();
        }
        else
        {
            return datatables()->of(User::query()->role($role))->toJson();
        }
    }

    public function index($role, $isArchive = false)
    {
        $isArchive = !empty($isArchive) ? 1:0;
        return view('admin.user', compact('isArchive', 'role'));
    }

    public function form($role, $id = 0)
    {
        $data = User::find($id);
        return view('admin.user_form', compact('data', 'role'));
    }

    public function save(UserPostRequest $request, $role, $id = 0)
    {
        $data = User::saveData($request, $role, $id);
        \Notify::success('Сохранено');
        return redirect('admin/users/' . $role . '/form/' . object_get($data, 'id'));
    }

    public function destroy($role, $id)
    {
        $data = User::find($id);
        if(!empty($data))
        {
            $data->delete();
            \Notify::success('Отправлено в архив');
        }
        return redirect('admin/users/' . $role);
    }

    public function restore($role, $id)
    {
        $data = User::withTrashed()->find($id);

        if(!empty($data))
        {
            $data->restore();
            \Notify::success('Восстановлено');
        }

        return redirect('/admin/users/' . $role);
    }
}
