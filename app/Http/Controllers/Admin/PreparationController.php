<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Preparation;
use App\Menu;

class PreparationController extends Controller
{
    public function __construct()
    {
        //parent::__construct();
        \App::setLocale('ru');
    }

    public function ajax(Request $request)
    {
        $preparation = Preparation::query();

        if($request->input('isArchive'))
        {
            return datatables()->of($preparation->onlyTrashed()
            )->toJson();
        }
        else
        {
            return datatables()->of($preparation)->toJson();
        }
    }

    public function index($isArchive = false)
    {
        $isArchive = !empty($isArchive) ? 1:0;
        return view('admin.preparation', compact('isArchive'));
    }

    public function form($id = 0)
    {
        $preparation = Preparation::find($id);
        $menuTree = Menu::selectTree();
        return view('admin.preparation_form', compact('preparation', 'menuTree'));
    }

    public function save(Request $request, $id = 0)
    {
        $preparation = Preparation::saveData($request, $id);
        \Notify::success('Сохранено');
        return redirect('admin/preparation/form/' . object_get($preparation, 'id'));
    }

    public function destroy($id)
    {
        $preparation = Preparation::find($id);
        if(!empty($preparation)) $preparation->delete();
        \Notify::success('Отправлено в архив');
        return redirect('admin/preparation');
    }

    public function restore($group_id, $id)
    {
        $preparation = Preparation::withTrashed()->find($id);

        if(!empty($preparation))
        {
            $preparation->restore();
            \Notify::success('Восстановлено');
        }

        return redirect('/admin/preparation');
    }
}
