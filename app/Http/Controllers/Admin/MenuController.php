<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;

class MenuController extends Controller
{
    public function __construct()
    {
        \App::setLocale('ru');
    }

    public function ajax(Request $request, $subid)
    {
        $menu = Menu::query()->whereSubid($subid);

        if($request->input('isArchive'))
        {
            return datatables()->of($menu->onlyTrashed()
            )->toJson();
        }
        else
        {
            return datatables()->of($menu)->toJson();
        }
    }

    public function index($subid = 0, $isArchive = false)
    {
        $isArchive = !empty($isArchive) ? 1:0;
        $menu = Menu::find($subid);

        return view('admin.menu', compact('subid', 'menu', 'isArchive'));
    }

    public function form($subid = 0, $id = 0)
    {
        $menu = Menu::find($id);
        $submenu = Menu::find($subid);
        $lastPosition = Menu::lastPossition($subid);

        return view('admin.menu_form', compact('subid', 'menu', 'submenu', 'lastPosition'));
    }

    public function save(Request $request, $subid = 0, $id = 0)
    {
        $menu = Menu::saveData($request, $subid, $id);
        \Notify::success('Сохранено');
        return redirect('admin/menu/' . $subid . '/form/' . object_get($menu, 'id'));
    }

    public function destroy($subid = 0, $id)
    {
        $menu = Menu::find($id);

        if(!empty($menu))
        {
            $menu->sub()->delete();
            $menu->delete();
            \Notify::success('Отправлено в архив');
        }

        return redirect('admin/menu/' . $subid);
    }

    public function restore($subid = 0, $id)
    {
        $menu = Menu::withTrashed()->find($id);

        if(!empty($menu))
        {
            $menu->sub()->restore();
            $menu->restore();
            \Notify::success('Восстановлено');
        }

        return redirect('admin/menu/' . $subid);
    }
}
