<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Menu;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $current_lang = null;

    public function __construct()
    {
        $this->current_lang = \LaravelLocalization::getCurrentLocale();

        view()->share([
            'current_lang' => $this->current_lang,
            'main_menu' => Menu::whereSubid(0)->orderBy('position')->get()
        ]);
    }
}
