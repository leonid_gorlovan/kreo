<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->segment('5');

        $rules['name'] = 'required|max:255|' . Rule::unique('users')->ignore($id, 'id');
        $rules['email'] = 'required|email|' . Rule::unique('users')->ignore($id, 'id');

        if($id > 0)
        {
            $rules['password'] = 'confirmed';
        }
        else
        {
            $rules['password'] = 'required|confirmed';
        }

        return $rules;
    }
}
