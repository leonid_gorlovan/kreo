<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Image;

class InfoBlock extends Model
{
    use SoftDeletes, \Dimsav\Translatable\Translatable;

    protected $dates = ['deleted_at'];
    protected $fillable = ['*'];
    public $translatedAttributes = ['title', 'link', 'description', 'seo_title', 'seo_description', 'seo_keywords'];

    public static function lastPossition()
    {
        $position = self::orderBy('position', 'desc')->first();
        $position = object_get($position, 'position', 0);
        return (int) $position + 1;
    }

    public static function saveData($request, $id)
    {   
        $data = self::findOrNew($id);
        $data->title = $request->input('title_ua');
        $data->link = $request->input('link_ua');
        $data->description = $request->input('description_ua');
        $data->position = (int) $request->input('position');
        $data->seo_title = $request->input('seo_title_ua');
        $data->seo_description = $request->input('seo_description_ua');
        $data->seo_keywords = $request->input('seo_keywords_ua');

        //Image

        $path = public_path('uploads/infoblock');

        if(!file_exists($path))
        {
            \File::makeDirectory($path, 0777, true);
        }

        if($request->hasFile('image'))
        {
            @unlink($path . '/' . $data->image);

            $file = $request->file('image');
            $file_name = str_random(32) . '.' . $file->getClientOriginalExtension();
            //$file->storeAs('/content/' . $type, $file_name);

            $image = Image::make($file);
            $image->resize(2000, 2000, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            \Storage::disk('public_uploads')->put('infoblock/' . $file_name, (string) $image->encode());

            $data->image = $file_name;
        }
        
        //Ru

        $data->translateOrNew('ru')->title = $request->input('title_ru');
        $data->translateOrNew('ru')->link = $request->input('link_ru');
        $data->translateOrNew('ru')->description = $request->input('description_ru');
        $data->translateOrNew('ru')->seo_title = $request->input('seo_title_ru');
        $data->translateOrNew('ru')->seo_description = $request->input('seo_description_ru');
        $data->translateOrNew('ru')->seo_keywords = $request->input('seo_keywords_ru');

        //En

        $data->translateOrNew('en')->title = $request->input('title_en');
        $data->translateOrNew('en')->link = $request->input('link_en');
        $data->translateOrNew('en')->description = $request->input('description_en');
        $data->translateOrNew('en')->seo_title = $request->input('seo_title_en');
        $data->translateOrNew('en')->seo_description = $request->input('seo_description_en');
        $data->translateOrNew('en')->seo_keywords = $request->input('seo_keywords_en');

        ////

        $data->save();

        return $data;
    }
}
