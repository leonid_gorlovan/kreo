<?php namespace App\Helpers;

use Str;

class Translit {

	public static function in($str) 
	{
	    return strtr($str, Translit::listLetters());
	}
	
	public static function out($str) 
	{
	    return strtr($str,array_flip( Translit::listLetters()));
	}
	
	public static function slug($str, $separator = '-')
	{
		return str_slug(strtr($str, Translit::listLetters()), $separator);
	}
	
	// public static function slug($title, $separator = '-')
	// {
	// 	$title = strtr($title, Translit::listLetters());
	// 	$title = Str::ascii($title);
 //        // Convert all dashes/underscores into separator
 //        $flip = $separator == '-' ? '_' : '-';
 //        $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);
 //        // Remove all characters that are not the separator, letters, numbers, or whitespace.
 //        $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', $title);
 //        // Replace all separator characters and whitespace by a single separator
 //        $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);
 //        return trim($title, $separator);
	// }
	
	public static function listLetters()
	{
		$tr = array(
		    "А"=>"A",
		    "Б"=>"B",
		    "В"=>"V",
		    "Г"=>"G",
		    "Ґ"=>"G",
		    "Д"=>"D",
		    "Е"=>"E",
		    "Є"=>"Ye",
		    "Ж"=>"Zh",
		    "З"=>"Z",
		    "И"=>"I",
		    "І"=>"I",
		    "Ї"=>"Yi",
		    "Й"=>"Y",
		    "К"=>"K",
		    "Л"=>"L",
		    "М"=>"M",
		    "Н"=>"N",
		    "О"=>"O",
		    "П"=>"P",
		    "Р"=>"R",
		    "С"=>"S",
		    "Т"=>"T",
		    "У"=>"U",
		    "Ф"=>"F",
		    "Х"=>"Kh",
		    "Ц"=>"Ts",
		    "Ч"=>"Ch",
		    "Ш"=>"Sh",
		    "Щ"=>"Shch",
		    "Ъ"=>"",
		    "Ы"=>"Y",
		    "Ь"=>"",
		    "Э"=>"",
		    "Ю"=>"Yu",
		    "Я"=>"Ya",
		    
		    "а"=>"a",
		    "б"=>"b",
		    "в"=>"v",
		    "г"=>"g",
		    "ґ"=>"g",
		    "д"=>"d",
		    "е"=>"e",
		    "є"=>"ie",
		    "ж"=>"zh",
		    "з"=>"z",
		    "і"=>"i",
		    "ї"=>"yi",
		    "и"=>"i",
		    "й"=>"y",
		    "к"=>"k",
		    "л"=>"l",
		    "м"=>"m",
		    "н"=>"n",
		    "о"=>"o",
		    "п"=>"p",
		    "р"=>"r",
		    "с"=>"s",
		    "т"=>"t",
		    "у"=>"u",
		    "ф"=>"f",
		    "х"=>"kh",
		    "ц"=>"ts",
		    "ч"=>"ch",
		    "ш"=>"sh",
		    "щ"=>"shch",
		    "ъ"=>"",
		    "ы"=>"y",
		    "ь"=>"",
		    "э"=>"e",
		    "ю"=>"yu",
		    "я"=>"ya",
		);
		
		return $tr;
	}

}