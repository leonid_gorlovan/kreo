<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoBlockTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['*'];
}
