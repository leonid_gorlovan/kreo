<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreparationTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['*'];
}
