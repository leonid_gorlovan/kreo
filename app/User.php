<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function saveData($request, $role, $id)
    {
        $password = $request->input('password');

        $data = self::findOrNew($id);
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        if(!empty($password)) $data->password = \Hash::make($password);
        $data->save();

        $data->syncRoles([$role]);

        return $data;
    }
}
