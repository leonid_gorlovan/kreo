<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Translit;
use Image;

class Preparation extends Model
{
    use SoftDeletes, \Dimsav\Translatable\Translatable;

    protected $dates = ['deleted_at'];
    protected $fillable = ['*'];
    public $translatedAttributes = ['slug', 'title', 'description', 'text', 'seo_title', 'seo_description', 'seo_keywords'];

    public static function saveData($request, $id)
    {   
        $title_ua = $request->input('title_ua');
        $slug_ua = $request->input('slug_ua');
        $title_ru = $request->input('title_ru');
        $slug_ru = $request->input('slug_ru');
        $title_en = $request->input('title_en');
        $slug_en = $request->input('slug_en');

        $data = self::findOrNew($id);

        if(empty($slug_ua))
        {
            $data->slug = Translit::slug($title_ua);
        }
        else
        {
            $data->slug = Translit::slug($slug_ua);
        }
        
        $data->title = $title_ua;
        $data->description = $request->input('description_ua');
        $data->text = $request->input('text_ua');
        $data->seo_title = $request->input('seo_title_ua');
        $data->seo_description = $request->input('seo_description_ua');
        $data->seo_keywords = $request->input('seo_keywords_ua');

        //Image

        $path = public_path('uploads/preparation');

        if(!file_exists($path))
        {
            \File::makeDirectory($path, 0777, true);
        }

        if($request->hasFile('image'))
        {
            @unlink($path . '/' . $data->image);

            $image = $request->file('image');
            $image_name = str_random(32) . '.' . $image->getClientOriginalExtension();
            //$file->storeAs('/content/' . $type, $file_name);

            $image = Image::make($image);
            $image->resize(2000, 2000, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            \Storage::disk('public_uploads')->put('preparation/' . $image_name, (string) $image->encode());

            $data->image = $image_name;
        }

        if($request->hasFile('file'))
        {
            @unlink($path . '/' . $data->file);

            $file = $request->file('file');
            $file_name = str_random(32) . '.' . $file->getClientOriginalExtension();
            \Storage::disk('public_uploads')->putFileAs('preparation', $file, $file_name);

            $data->file = $file_name;
        }
        
        //Ru

        if(empty($slug_ru))
        {
            $data->translateOrNew('ru')->slug = Translit::slug($title_ru);
        }
        else
        {
            $data->translateOrNew('ru')->slug = Translit::slug($slug_ru);
        }

        $data->translateOrNew('ru')->title = $title_ru;
        $data->translateOrNew('ru')->description = $request->input('description_ru');
        $data->translateOrNew('ru')->text = $request->input('text_ru');
        $data->translateOrNew('ru')->seo_title = $request->input('seo_title_ru');
        $data->translateOrNew('ru')->seo_description = $request->input('seo_description_ru');
        $data->translateOrNew('ru')->seo_keywords = $request->input('seo_keywords_ru');

        //En

        if(empty($slug_en))
        {
            $data->translateOrNew('en')->slug = Translit::slug($title_en);
        }
        else
        {
            $data->translateOrNew('en')->slug = Translit::slug($slug_en);
        }

        $data->translateOrNew('en')->title = $title_en;
        $data->translateOrNew('en')->description = $request->input('description_en');
        $data->translateOrNew('en')->text = $request->input('text_en');
        $data->translateOrNew('en')->seo_title = $request->input('seo_title_en');
        $data->translateOrNew('en')->seo_description = $request->input('seo_description_en');
        $data->translateOrNew('en')->seo_keywords = $request->input('seo_keywords_en');

        ////

        $data->save();

        return $data;
    }

    // public function getFullSlugAttribute()
    // {
    //     //$group_slug = object_get($this, 'group.slug');
    //     //return url($group_slug . '/' . $this->slug);

    //     $current_lang = \LaravelLocalization::getCurrentLocale();
    //     $page = request('page', 1);
    //     $link = \LaravelLocalization::getLocalizedURL($current_lang, 'preparation/' . object_get($this, 'slug:' . $current_lang));

    //     if(request()->has('page')) $link = $link . '?page=' . $page;

    //     return $link;
    // }

    public function getFullSlugAttribute()
    {
        $menu = \App\Menu::whereTmpl('preparations')->first();
        $current_lang = \LaravelLocalization::getCurrentLocale();
        $page = request('page', 1);

        $link = \LaravelLocalization::getLocalizedURL($current_lang, object_get($menu, 'slug') . '/' . object_get($this, 'slug:' . $current_lang));

        if(request()->has('page')) $link = $link . '?page=' . $page;

        return $link;
    }
}
