<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Translit;

class Menu extends Model
{
    use SoftDeletes, \Dimsav\Translatable\Translatable;
    
    protected $dates = ['deleted_at'];
    protected $fillable = ['*'];
    public $translatedAttributes = ['slug', 'title', 'seo_title', 'seo_description', 'seo_keywords'];
    
    public function sub()
    {
        return $this->hasMany(Menu::class, 'subid', 'id');
    }

    public function revsub()
    {
        return $this->hasMany(Menu::class, 'id', 'subid');
    }

    public function content()
    {
        return $this->hasMany(Article::class, 'group_id', 'id');
    }

    public static function lastPossition($subid)
    {
        $position = self::whereSubid($subid)->orderBy('position', 'desc')->first();
        $position = object_get($position, 'position', 0);
        return (int) $position + 1;
    }

    public static function saveData($request, $subid, $id)
    {   
        $title_ua = $request->input('title_ua');
        $slug_ua = $request->input('slug_ua');
        $title_ru = $request->input('title_ru');
        $slug_ru = $request->input('slug_ru');
        $title_en = $request->input('title_en');
        $slug_en = $request->input('slug_en');

        $data = self::findOrNew($id);
        $data->subid = (int) $subid;
        $data->tmpl = $request->input('tmpl');

        if(empty($slug_ua))
        {
            $data->slug = Translit::slug($title_ua);
        }
        else
        {
            $data->slug = Translit::slug($slug_ua);
        }
        
        $data->title = $title_ua;
        $data->position = (int) $request->input('position');
        $data->order = $request->input('order');
        $data->seo_title = $request->input('seo_title_ua');
        $data->seo_description = $request->input('seo_description_ua');
        $data->seo_keywords = $request->input('seo_keywords_ua');
        //Ru

        if(empty($slug_ru))
        {
            $data->translateOrNew('ru')->slug = Translit::slug($title_ru);
        }
        else
        {
            $data->translateOrNew('ru')->slug = Translit::slug($slug_ru);
        }

        $data->translateOrNew('ru')->title = $title_ru;
        $data->translateOrNew('ru')->seo_title = $request->input('seo_title_ru');
        $data->translateOrNew('ru')->seo_description = $request->input('seo_description_ru');
        $data->translateOrNew('ru')->seo_keywords = $request->input('seo_keywords_ru');

        //En

        if(empty($slug_en))
        {
            $data->translateOrNew('en')->slug = Translit::slug($title_en);
        }
        else
        {
            $data->translateOrNew('en')->slug = Translit::slug($slug_en);
        }

        $data->translateOrNew('en')->title = $title_en;
        $data->translateOrNew('en')->seo_title = $request->input('seo_title_en');
        $data->translateOrNew('en')->seo_description = $request->input('seo_description_en');
        $data->translateOrNew('en')->seo_keywords = $request->input('seo_keywords_en');

        ////

        $data->save();

        return $data;
    }

    public static function selectTree()
    {
        $out = array();
        $data = self::whereSubid(0)->get();

        if(!empty($data))
        {
            // foreach ($data as $value) 
            // {
            //     $sub = array();

            //     foreach($value->sub as $sub_value)
            //     {
            //         $sub[$sub_value->id] = $sub_value->title;
            //     }

            //     $out[$value->title] = $sub;
            // }

            foreach ($data as $value) 
            {
                $out[$value->id] = $value->title;

                $sub = array();

                foreach($value->sub as $sub_value)
                {
                    $out[$sub_value->id] = ' -- ' . $sub_value->title;
                }
            }
        }

        return $out;
    }

    public function getFullSlugAttribute()
    {
        $current_lang = \LaravelLocalization::getCurrentLocale();
        $page = request('page', 1);

        $data = Menu::whereId($this->subid)->first();
        
        $link = array();
        $link[] = object_get($data, 'slug:' . $current_lang);
        $link[] = object_get($this, 'slug:' . $current_lang);
        $link = implode('/', $link);

        return \LaravelLocalization::getLocalizedURL($current_lang, $link);
    }
}
