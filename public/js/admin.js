$(document).ready( function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    tinymce.init({
        selector: '.tinymce',
        height : 450,
        plugins: 'table wordcount code',
        menu: {
            edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Insert', items: 'link media | template hr'},
            view: {title: 'View', items: 'visualaid'},
            format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
            table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'}
        }
    });

    tinymce.init({
        selector: '.tinymceSmall',
        height : 350,
        plugins: 'table wordcount code',
        menu: {
            edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Insert', items: 'link media | template hr'},
            view: {title: 'View', items: 'visualaid'},
            format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
            table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'}
        }
    });

    // isPaid

    // if( $('input[name="isPaid"]').is(":checked") )
    // {
    //     $('.position-radio').removeAttr('disabled');
    // }

    ///position-radio

    // Alerts

    setTimeout(function() {
		$('.alerts-box').fadeOut();
	}, 5000);

	$(document).on('click', '.alert, .alerts-box', function() {
		$('.alerts-box').fadeOut();
    });

    // Confirm

    $(document).on('click', '.btn-archive', function() {
        return confirm('Вы действительно хотите переместить в архив запись ?');
    });

    $(document).on('click', '.del-img-confirm', function() {
        return confirm('Вы действительно хотите удалить изображение ?');
    });

    // Select

    $('select').select2();

    $('select.tag-select').select2({
        ajax: {
            method: "POST",
            url: '/admin/tags/json/list',
            delay: 250
        }
    });

    // Calendar

    $('.datetimepickerDate').datetimepicker({
        locale: 'ru',
        format: 'YYYY-MM-DD HH:mm:ss'
    });

    // New Tag

    $(document).on('click', '#btn-modal-tag', function() {
        $('#newTagModal').modal('toggle');
        $('#tagModalError-input').val('');
        $('#tagModalError-title').text('');
        return false;
    });

    $(document).on('submit', '#form-save-tag', function(e){
        e.preventDefault();

        $('#tagModalError-title').text('');

        $.ajax({
            method: 'POST',
            url: '/admin/tags/json/create',
            data: $('#form-save-tag').serialize(),
            success: function() {
                $('#newTagModal').modal('toggle');
                $('#tagModalError-input').val('');
            },
            error: function(data) {
                if(data.responseJSON.errors)
                {
                    $('#tagModalError-title').text(data.responseJSON.errors.title[0]);
                }
            }
        });

        return false;
    });

});
