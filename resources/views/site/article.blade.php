@extends('site.tmpl')
@section('content')

<section class="blog-single-wrapper ">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="post-details">
                    @if (object_get($article, 'image'))
                    <div class="post-head">
                        <img src="/img/cache/f920x520/articles/{{ object_get($article, 'image') }}" alt="">
                    </div>
                    @endif
                    <div class="post-content">
                        <div class="post-header">
                            <h3>{!! object_get($article, 'title:' . $current_lang) !!}</h3>
                        </div>
                        <div class="post-body">
                            {!! object_get($article, 'text:' . $current_lang) !!}
                        </div>
                    </div>
                </div><!-- ends: .post-details -->

                <div class="related-post m-top-10">
                    <div class="related-post--title">
                        <h4>{{ trans('site.related_materials') }}</h4>
                    </div>
                    <div class="row">
                        @foreach ($randomArticles as $itemRandom)
                            <div class="col-lg-4 col-sm-6">
                                <div class="single-post">
                                    <img src="/img/cache/f360x230/articles/{{ object_get($itemRandom, 'image') }}" alt="">
                                    <h6><a href="{{ object_get($itemRandom, 'full_slug') }}">{!! object_get($itemRandom, 'title:' . $current_lang) !!}</a></h6>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div><!-- ends: .related-post -->

            </div><!-- ends: .col-lg-8 -->
        </div>
    </div>
</section><!-- ends: .blog-wrapper -->

@endsection