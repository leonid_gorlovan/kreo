<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {!! SEOMeta::generate() !!}
	{!! OpenGraph::generate() !!}
	{!! Twitter::generate() !!}
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,900|Mirza:400,700&amp;subset=arabic"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Allura" rel="stylesheet">

    <!-- inject:css-->
    <link rel="stylesheet" href="/css/plugin.min.css?v=1">
    <link rel="stylesheet" href="/style.css?v=1">
    <link rel="stylesheet" href="/css/user.css?v=1">
    <!-- endinject -->
</head>
<body style="background: url('/img/1.jpg'); background-repeat: no-repeat; background-attachment: fixed; background-size: cover; ">

<!-- header area -->
<section class="header header--2">

    {{-- TopBlue Line --}}
    <div class="top_bar top--bar2 d-flex align-items-center bg-primary" style="min-height: 2em !important; ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex topbar_content justify-content-between">
                        <div class="top_bar--lang align-self-center order-2">
                            <div class="dropdown">
                                <div class="dropdown-toggle d-flex align-items-center" id="dropdownMenuButton1"
                                     role="menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="lang">{{ LaravelLocalization::getCurrentLocaleName() }} &nbsp;</span>

                                    <span class="la la-angle-down"></span>
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <a class="dropdown-item"  data-lang="{{ $localeCode }}" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                                {{ $properties['native'] }}
                                            </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="top_bar--info order-0 d-none d-lg-block align-self-center">
                            <ul>
                                <li>
                                    <span class="la la-envelope"></span>
                                    <p>info@creopharm.com.ua</p>
                                </li>
                            </ul>
                        </div>
                        <div class="top_bar--social">
                            <ul>
                                <!--<li><a href="#"><span class="fab fa-facebook-f"></span></a></--li>
                                <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                                <li><a href="#"><span class="fab fa-vimeo-v"></span></a></li>
                                <li><a href="#"><span class="fab fa-linkedin-in"></span></a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- start menu area -->
    <div class="menu_area menu1">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light px-0 pt-0 pb-0">
                <a class="navbar-brand order-sm-1 order-1" href="{{ \LaravelLocalization::getLocalizedURL($current_lang, '/') }}"><img src="/img/Creo-Pharm.png" alt=""/></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="la la-bars"></span>
                </button>

                <div class="collapse navbar-collapse order-md-1" id="navbarSupportedContent2">
                    <ul class="navbar-nav m-auto">
                        @foreach ($main_menu as $item_main_menu)
                            @if (!empty($item_main_menu->sub) && count($item_main_menu->sub) > 0)
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="{{ object_get($item_main_menu, 'full_slug', '#') }}" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">{!! object_get($item_main_menu, 'title:' . $current_lang) !!}</a>
                                    <div class="dropdown-menu">
                                        @foreach ($item_main_menu->sub as $sub_main_menu)
                                            <a class="dropdown-item" href="{{  object_get($sub_main_menu, 'full_slug', '#') }}">{!! object_get($sub_main_menu, 'title:' . $current_lang) !!}</a>
                                        @endforeach
                                    </div>
                                </li>
                            @else
                                <li class="nav-item ">
                                    <a class="nav-link" href="{{  object_get($item_main_menu, 'full_slug', '#') }}">{!! object_get($item_main_menu, 'title:' . $current_lang) !!}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    <!-- end: .navbar-nav -->
                </div>
            </nav>
        </div>
    </div>
    <!-- end menu area -->

</section><!-- end: .header -->

@yield('content')

<footer class="footer5 footer--bw">
    <div class="footer__small text-center">
        <p>©2019 CREO PHARM. All rights reserved.</p>
    </div><!-- ends: .footer__small -->
</footer>

<div class="go_top">
    <span class="la la-angle-up"></span>
</div>
<!-- inject:js-->
<script src="/js/plugins.min.js"></script>
<script src="/js/script.min.js"></script>
<!-- endinject-->
</body>
</html>