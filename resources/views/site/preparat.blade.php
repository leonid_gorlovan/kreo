@extends('site.tmpl')
@section('content')

<section class="blog-single-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="post-details">
                    <div class="post-content">
                        <div class="post-header">
                            <h1>{!! object_get($preparation, 'title:' . $current_lang) !!}</h1>
                        </div>
                        <div class="post-body">
                            <div class="row">
                                <div class="col-lg-5">
                                    <img src="/img/cache/f920x520/preparation/{{ object_get($preparation, 'image') }}" alt="">
                                </div>
                                <div class="col-lg-7">
                                    {!! object_get($preparation, 'description:' . $current_lang) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    {!! object_get($preparation, 'text:' . $current_lang) !!}
                                </div>
                            </div>
                            
                            @if (object_get($preparation, 'file'))
                            <div class="row">
                                <div class="col-lg-12 text-center m-top-10">
                                    <a href="/uploads/preparation/{{ object_get($preparation, 'file') }}" class="btn btn-primary blue-bg">{{ trans('site.download_manual') }}</a>
                                </div>
                            </div>
                            @endif
                            
                            <div class="related-post m-top-10">
                                <div class="related-post--title">
                                    <h4>{{ trans('site.related_materials') }}</h4>
                                </div>

                                <div class="row">
                                    @foreach ($randomArticles as $itemRandom)
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="single-post">
                                                <img src="/img/cache/f360x230/preparation/{{ object_get($itemRandom, 'image') }}" alt="">
                                                <h6><a href="{{ object_get($itemRandom, 'full_slug') }}">{!! object_get($itemRandom, 'title:' . $current_lang) !!}</a></h6>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- ends: .post-details -->

            </div><!-- ends: .col-lg-8 -->
        </div>
    </div>
</section><!-- ends: .blog-wrapper -->

@endsection