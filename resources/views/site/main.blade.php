@extends('site.tmpl')
@section('content')

<div class="p-top-10 p-bottom-10">
    <div class="m-bottom-10">
        <div class="divider divider-simple text-center">
            <h3>{{ trans('site.title_site') }}</h3>
        </div>

    </div>

    <div class="card-style-eleven">
        <div class="container">
            <div class="row">
                @foreach ($infoBlock as $item_infoBlock)
                    <div class="col-lg-4 col-md-6">

                        <div class="card card--eleven" style="margin-bottom:0;">
                            <figure>
                                <img src="/img/cache/f360x350/infoblock/{{ object_get($item_infoBlock, 'image') }}" alt="">
                            </figure>
                            <div class="card-body text-center">
                                <div class="card-contents">
                                    <div class="content-top">
                                        <span><i class="la la-area-chart"></i></span>
                                        <h6>{{ object_get($item_infoBlock, 'title:' . $current_lang) }}</h6>
                                    </div>
                                    <div class="content-bottom">
                                        <p>{!! object_get($item_infoBlock, 'description:' . $current_lang) !!}</p>
                                        <a href="{{ object_get($item_infoBlock, 'link') }}" class="btn btn-secondary btn-sm blue-bg">{{ trans('site.see_details') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End: .card -->
    
                    </div>
                @endforeach
            </div><!-- ends: .row -->
        </div>
    </div><!-- ends: .card-style-eleven -->

</div><!-- ends: .section-padding -->

<div class="p-top-10 p-bottom-5 sectionbg">
    <div class="m-bottom-5">
        <div class="divider divider-simple text-center">
            <h3>{{ trans('site.see_details') }}:</h3>
        </div>
    </div>

    <section class="icon-boxes icon-box--eight">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">

                    <div class="icon-box-eight text-center text-md-left" style="margin-bottom: 1em;">
                        <span class="icon-square-sm" style="margin-bottom: 0.33rem;"><i class="la la-thumbs-up"></i></span>
                        <h6>{{ trans('site.high_quality_drugs') }}</h6>
                        <p>{{ trans('site.high_quality_drugs_text') }}</p>
                    </div><!-- ends: .icon-box -->

                </div><!-- ends: .col-lg-4 -->
                <div class="col-lg-4 col-md-6">

                    <div class="icon-box-eight text-center text-md-left">
                        <span class="icon-square-sm" style="margin-bottom: 0.33rem;"><i class="la la-bar-chart"></i></span>
                        <h6>{{ trans('site.сonfirmed_research') }}</h6>
                        <p>{{ trans('site.сonfirmed_research_text') }}</p>
                    </div><!-- ends: .icon-box -->

                </div><!-- ends: .col-lg-4 -->
                <div class="col-lg-4 col-md-6">

                    <div class="icon-box-eight text-center text-md-left">
                        <span class="icon-square-sm" style="margin-bottom: 0.33rem;"><i class="la la-money"></i></span>
                        <h6>{{ trans('site.weighted_value') }}</h6>
                        <p>{{ trans('site.weighted_value_text') }}</p>
                    </div><!-- ends: .icon-box -->

                </div><!-- ends: .col-lg-4 -->
            </div><!-- ends: .row -->
        </div>
    </section><!-- ends: .icon-boxes -->

</div><!-- ends: .section-padding5 -->

<section class="carousel-wrapper p-top-10 p-bottom-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 margin-md-10">

                <div class="divider text-left">
                    <h3 class="color-dark">{{ trans('site.actual_today') }}</h3>
                    <p>{{ trans('site.actual_today_text') }}</p>
                </div>

                <div class="mt-4">
					
                    <ul class="arrow--list2">

                        <li class="list-item arrow-list2 d-flex align-items-center"><span><i
                            class="la la-angle-right"></i></span>{{ trans('site.actual_today_list1') }}
                        </li>


                        <li class="list-item arrow-list2 d-flex align-items-center"><span><i
                            class="la la-angle-right"></i></span>{{ trans('site.actual_today_list2') }} 
                        </li>


                        <li class="list-item arrow-list2 d-flex align-items-center"><span><i
                            class="la la-angle-right"></i></span>{{ trans('site.actual_today_list3') }}
                        </li>


                        <li class="list-item arrow-list2 d-flex align-items-center"><span><i
                            class="la la-angle-right"></i></span>{{ trans('site.actual_today_list4') }}
                        </li>
					 
                    </ul><!-- ends: .arrow--list2 -->

                </div>
                <div class="mt-5">
                    {{-- <a href="" class="btn btn-outline-secondary outline-thick-secondary">See Pricing</a> --}}
                </div>
            </div><!-- ends: .col-lg-5 -->
            <div class="col-lg-6 offset-lg-1">

                <div class="carousel-four owl-carousel">
                    @foreach ($lastPreparation as $item_lastPreparation)
                        <div class="carousel-single">
                            <div class="card card-shadow card--eight">
                                <figure>
                                    <img src="/img/cache/f555x350/preparation/{{ object_get($item_lastPreparation, 'image') }}" alt="">
                                </figure>
                                <div class="card-body text-center">
                                    <h5><a href="{{ object_get($item_lastPreparation, 'full_slug') }}">{{ object_get($item_lastPreparation, 'title:' . $current_lang) }}</a></h5>
                                    <p>{{ object_get($item_lastPreparation, 'description:' . $current_lang) }}</p>
                                    <a href="{{ object_get($item_lastPreparation, 'full_slug') }}" class="btn btn-primary blue-bg">{{ trans('site.see_details') }}</a>
                                </div>
                            </div><!-- End: .card -->
                        </div><!-- ends: .carousel-single -->
                    @endforeach
                </div><!-- ends: .carousel-four -->

            </div>
        </div>
    </div>
</section><!-- ends: .carousel-wrapper -->

<section class="p-top-10 p-bottom-10">

    <section class="form-wrapper contact--from5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="divider text-center m-bottom-5">
                        <h1 class="color-dark m-0">{{ trans('site.feedback') }}</h1>
                    </div>

                    <div class="form-wrapper">
                        
                        {!! NoCaptcha::renderJs($current_lang, false, 'feedbackForm') !!}
                       
                        @if (session()->has('message'))
                            <div>
                                {{ trans('site.message_send_feedback') }}
                            </div>
                        @endif

                        @if ($current_lang == 'ua')
                        <form action="/sendfeedback" method="POST" id="feedbackForm">
                        @else
                        <form action="/{{ $current_lang }}/sendfeedback" method="POST" id="feedbackForm"> 
                        @endif
                            @csrf
                            <div class="row">
                                <div class="col-lg-4 col-md-6 m-bottom-5">
                                    <input type="text" name="name" placeholder="{{ trans('site.name') }}" class="form-control">
                                    @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <div class="col-lg-4 col-md-6 m-bottom-5">
                                    <select name="type" class="form-control" id="exampleFormControlSelect1">
                                        <option>{{ trans('site.type_1') }}</option>
                                        <option>{{ trans('site.type_2') }}</option>
                                        <option>{{ trans('site.type_3') }}</option>
                                    </select>

                                    @if ($errors->has('type'))
                                        <span class="text-danger">{{ $errors->first('type') }}</span>
                                    @endif
                                </div>
                                <div class="col-lg-4 m-bottom-5">
                                    <input type="email" name="email" placeholder="Email" class="form-control">
                                    @if ($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="col-lg-12 m-bottom-5">
                                    <textarea name="text" class="form-control" rows="7" placeholder="{{ trans('site.message') }}"></textarea>
                                    @if ($errors->has('text'))
                                        <span class="text-danger">{{ $errors->first('text') }}</span>
                                    @endif
                                </div>
                                <div class="col-lg-12 text-center m-top-5">
                                    {!! NoCaptcha::displaySubmit('feedbackForm', trans('site.submit'), ['class' => 'btn btn-primary blue-bg', 'data-badge' => 'bottomleft']) !!}
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div><!-- end: .form-wrapper -->

                </div>
            </div>
        </div>
    </section><!-- ends: .form-wrapper -->

</section>

@endsection