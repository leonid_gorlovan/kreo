@extends('site.tmpl')
@section('content')

<section class="blog-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="blog-posts">
                    @foreach ($preparations as $item)
                        <div class="blog-single">
                            <div class="card post--card post--card2 ">
                                <figure>
                                    <a href="{{ $link }}/{{ object_get($item, 'slug') }}"><img src="/img/cache/f920x520/preparation/{{ object_get($item, 'image') }}" alt=""></a>
                                </figure>
                                <div class="card-body">
                                    <h5><a href="{{ $link }}/{{ object_get($item, 'slug') }}">{{ object_get($item, 'title') }}</a></h5>
                                    <p>{{ object_get($item, 'description') }}</p>
                                </div>
                            </div><!-- End: .card -->
    
                        </div><!-- ends: .blog-single -->
                    @endforeach
                </div>

                <div class="m-top-10">

                    <div class="pagination-area">
                        <nav aria-label="Page navigation pagination-left">
                            {{ $preparations->onEachSide(2)->links('vendor.pagination.bootstrap-4') }}
                        </nav>
                    </div><!-- ends: .pagination-wrapper -->

                </div>
            </div><!-- ends: .col-lg-8 -->
        </div>
    </div>
</section>

@endsection