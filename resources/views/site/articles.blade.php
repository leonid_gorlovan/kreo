@extends('site.tmpl')
@section('content')

<section class="blog-wrapper section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="blog-posts">
                    @foreach ($articles as $item)
                        <div class="blog-single">
                            <div class="card post--card post--card2 ">
                                <figure>
                                    <a href="{{ object_get($item, 'full_slug') }}"><img src="/img/cache/f920x520/articles/{{ object_get($item, 'image') }}" alt=""></a>
                                </figure>
                                <div class="card-body">
                                    <h5><a href="{{ object_get($item, 'full_slug') }}">{{ object_get($item, 'title:' . $current_lang) }}</a></h5>
                                    <p>{{ object_get($item, 'description:' . $current_lang) }}</p>
                                </div>
                            </div><!-- End: .card -->
    
                        </div><!-- ends: .blog-single -->
                    @endforeach
                </div>

                <div class="m-top-50">

                    <div class="pagination-area">
                        <nav aria-label="Page navigation pagination-left">
                            {{ $articles->onEachSide(2)->links('vendor.pagination.bootstrap-4') }}
                        </nav>
                    </div><!-- ends: .pagination-wrapper -->

                </div>
            </div><!-- ends: .col-lg-8 -->
        </div>
    </div>
</section>

@endsection