@extends('adminlte::page')
@section('title', 'Новости')
@section('content_header')
<h1>Новости</h1>
@stop
@section('content')

    {!! Form::model($data, ['url' => '/admin/users/' . $role . '/save/' . object_get($data, 'id', 0)]) !!}

    <div class="box">
        <div class="box-header with-border">
            <a href="/admin/users/{{ $role }}" class="btn btn-primary"><i class="far fa-caret-square-left"></i></a>
            <button type="submit" class="btn btn-success pull-right"><i class="far fa-save"></i> Сохранить</button>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label><sup class="text-danger">*</sup> Имя</label>
                {!! Form::text('name', null, ['class' => 'form-control form-control-sm']) !!}
            </div>
            <div class="form-group">
                <label><sup class="text-danger">*</sup> Email</label>
                {!! Form::text('email', null, ['class' => 'form-control form-control-sm']) !!}
            </div>
            <div class="form-group">
                <label><sup class="text-danger">*</sup> Пароль</label>
                {!! Form::password('password', ['class' => 'form-control form-control-sm']) !!}
            </div>
            <div class="form-group">
                <label><sup class="text-danger">*</sup> Повт. Пароль</label>
                {!! Form::password('password_confirmation', ['class' => 'form-control form-control-sm']) !!}
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-block btn-success">Сохранить</button>

    {!! Form::close() !!}

@stop


