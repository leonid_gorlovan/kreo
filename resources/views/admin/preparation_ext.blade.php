<div class="form-group">
    <label><sup class="text-danger">*</sup> Название <small>({{ $lang }})</small></label>
    {!! Form::text('title_' . $lang, object_get($preparation, 'title:' . $lang), ['class' => 'form-control form-control-sm']) !!}
</div>

<div class="row">
    <div class="col-xs-8">

        @if ($lang == 'ua')
            <div class="form-group">
                <label>Изображение <small>(555x350; jpeg, png)</small></label>
    
                <div class="input-group">
                    <input type="file" name="image" class="file-upload-default">
                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                    <span class="input-group-btn">
                        <button class="file-upload-browse btn btn-info" type="button">Выбрать файл</button>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label>Инструкция <small>(файл для скачивания)</small></label>
    
                <div class="input-group">
                    <input type="file" name="file" class="file-upload-default">
                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                    <span class="input-group-btn">
                        <button class="file-upload-browse btn btn-info" type="button">Выбрать файл</button>
                    </span>
                </div>
            </div>
        @endif

        <div class="form-group">
            <label>Краткое содержание <small>({{ $lang }})</small></label>
            {!! Form::textarea('description_' . $lang, object_get($preparation, 'description:' . $lang), ['class' => 'form-control', 'rows' => 6]) !!}
        </div>
    </div>
    <div class="col-xs-4">
        @if(object_get($preparation, 'image'))
            <img src="/img/cache/f555x350/preparation/{{ object_get($preparation, 'image') }}?t={{ time() }}" alt="" class="img-responsive pull-right">
        @endif
    </div>
</div>

<div class="form-group">
    <label>Содержание <small>({{ $lang }})</small></label>
    {!! Form::textarea('text_' . $lang, object_get($preparation, 'text:' . $lang), ['class' => 'tinymce']) !!}
</div>

<hr />

<h2>SEO</h2>

<div class="form-group">
    <label>ЧПУ <small>({{ $lang }}, оставьте пустое поле если хотите сгенерировать из названия)</small></label>
    {!! Form::text('slug_' . $lang, object_get($preparation, 'slug:' . $lang), ['class' => 'form-control form-control-sm']) !!}
</div>

<div class="form-group">
    <label>Title <small>({{ $lang }})</small></label>
    {!! Form::text('seo_title_' . $lang, object_get($preparation, 'seo_title:' . $lang), ['class' => 'form-control form-control-sm']) !!}
</div>

<div class="form-group">
    <label>Description <small>({{ $lang }})</small></label>
    {!! Form::textarea('seo_description_' . $lang, object_get($preparation, 'seo_description:' . $lang), ['class' => 'form-control form-control-sm', 'rows' => '4'] ) !!}
</div>

<div class="form-group">
    <label>Keywords <small>({{ $lang }})</small></label>
    {!! Form::textarea('seo_keywords_' . $lang, object_get($preparation, 'seo_keywords:' . $lang), ['class' => 'form-control form-control-sm', 'rows' => '4'] ) !!}
</div>