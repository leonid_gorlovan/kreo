@extends('adminlte::page')
@section('title', 'Новости')
@section('content_header')
<h1>Препараты</h1>
@stop
@section('content')

    {!! Form::model($product, ['url' => '/admin/products/save/' . object_get($product, 'id', 0), 'files' => true]) !!}

    <div class="box">
        <div class="box-header with-border">
            <a href="/admin/products" class="btn btn-primary"><i class="far fa-caret-square-left"></i></a>
            <button type="submit" class="btn btn-success pull-right"><i class="far fa-save"></i> Сохранить</button>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label><sup class="text-danger">*</sup> Название</label>
                {!! Form::text('title', null, ['class' => 'form-control form-control-sm']) !!}
            </div>

            <div class="row">
                <div class="col-xs-8">
                    <div class="form-group">
                        <label>Изображение</label>

                        <div class="input-group">
                            <input type="file" name="image" class="file-upload-default">
                            <input type="text" class="form-control file-upload-info" disabled="">
                            <span class="input-group-btn">
                                <button class="file-upload-browse btn btn-info" type="button">Выбрать файл</button>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Документ для загрузки</label>

                        <div class="input-group">
                            <input type="file" name="pdf" class="file-upload-default">
                            <span class="input-group-btn">
                                <a href="/admin/products/download-file/1" class="btn btn-default">Скачать файл</a>
                            </span>
                            <input type="text" class="form-control file-upload-info" disabled="">
                            <span class="input-group-btn">
                                <button class="file-upload-browse btn btn-info" type="button">Выбрать файл</button>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Описание <small>(Список товаров)</small></label>
                        {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 6]) !!}
                    </div>
                </div>
                <div class="col-xs-4 text-right">
                    @if(object_get($product, 'image'))
                        <img src="/img/cache/r400/products/{{ object_get($product, 'image') }}?t={{ time() }}" alt="" class="img-responsive">

                        {{-- <a href="/admin/products/delete-img/{{ object_get($product, 'id') }}" class="btn btn-xs btn-danger del-img-confirm">Удалить Изображение</a> --}}
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label>Краткое содержание</label>
                {!! Form::textarea('text1', null, ['class' => 'tinymceSmall']) !!}
            </div>
            
            <div class="form-group">
                <label>Содержание</label>
                {!! Form::textarea('text2', null, ['class' => 'tinymce']) !!}
            </div>

            <hr />

            <h2>SEO</h2>

            <div class="form-group">
                <label>Title</label>
                {!! Form::text('seo_title', null, ['class' => 'form-control form-control-sm']) !!}
            </div>

            <div class="form-group">
                <label>Description</label>
                {!! Form::textarea('seo_description', null, ['class' => 'form-control form-control-sm', 'rows' => '4'] ) !!}
            </div>

            <div class="form-group">
                <label>Keywords</label>
                {!! Form::textarea('seo_keywords', null, ['class' => 'form-control form-control-sm', 'rows' => '4'] ) !!}
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-block btn-success">Сохранить</button>

    {!! Form::close() !!}

@stop


