@extends('adminlte::page')
@section('title', 'Статьи')
@section('content_header')
    @if (!empty($menu))
        <h1>Статьи - {{ object_get($menu, 'title') }}</h1>
    @else
        <h1>Статьи</h1>
    @endif
@stop
@section('content')

<div class="box">
    <div class="box-header with-border">
        <div class="row">
            <div class="col-xs-6">
                @if (!empty($menu))
                    <a href="/admin/menu/{{ $menu->subid }}" class="btn btn-primary"><i class="far fa-caret-square-left"></i></a>
                @endif
            </div>
            <div class="col-xs-6 text-right">
                <a href="/admin/group/{{ $group_id }}/articles/form/0" class="btn btn-primary"><i class="fas fa-plus"></i> Создать</a>
                @if($isArchive == 1)
                    <a href="/admin/group/{{ $group_id }}/articles" class="btn btn-warning"><i class="fas fa-list-ul"></i> Список</a>
                @else
                    <a href="/admin/group/{{ $group_id }}/articles/archive" class="btn btn-warning"><i class="fas fa-archive"></i> Архив</a>
                @endif
            </div>
        </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body">
        <table class="table table-md table-hover" id="table">
            <thead>
                <th>#</th>
                <th>Название</th>
                <th>Меню</th>
                <th class="edit"></th>
            </thead>
        </table>
    </div>
    <!-- /.box-body -->
</div>

@stop

@section('js')

    <script>
        $(document).ready( function () {
            $('#table').DataTable({
                "language": {
                    "url": "/vendor/adminlte/plugins/dataTableRus.json"
                },
                serverSide: true,
                ajax: {
                    url: "/admin/group/{{ $group_id }}/articles/ajax",
                    data: { isArchive: {{ $isArchive }} },
                    type: 'POST'
                },
                stateSave: true,
                "pageLength": 25,
                "aLengthMenu": [
                    [25, 50, 75, 100], [25, 50, 75, 100]
                ],
                "order": [
                    [ 0, "asc" ]
                ],
                "columns": [
                    { "data": "id" },
                    { "data": "title" },
                    { "data": "group.title" },
                    { "data": "" },
                ],
                columnDefs: [
                    {
                        "targets": 0,
                        "orderable": true,
                        "searchable": false,
                        "width": "80px"
                    },
                    {
                        "targets": 1,
                        "orderable": true,
                        "searchable": true,
                        mRender: function ( data, type, row ) {
                            return '<a href="/admin/group/{{ $group_id }}/articles/form/' + row.id + '">' + data + '</a>';
                        }
                    },
                    {
                        "targets": 2,
                        "orderable": false,
                        "searchable": false
                    },
                    {
                        "targets": 3,
                        "orderable": false,
                        "searchable": false,
                        "width": "40px",
                        "render": function ( data, type, row ) {
                            if({{ $isArchive }})
                            {
                                return '<a href="/admin/group/{{ $group_id }}/articles/restore/' + row.id + '" class="btn btn-success btn-xs"><i class="fa fa-undo"></i></a>';
                            }
                            else
                            {
                                return '<a href="/admin/group/{{ $group_id }}/articles/destroy/' + row.id + '" class="btn btn-warning btn-xs btn-archive"><i class="fa fa-archive"></i></a>';
                            }
                        }
                    }
                ],
            });
        });
    </script>

@stop
