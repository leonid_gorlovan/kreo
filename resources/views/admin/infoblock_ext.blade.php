<div class="form-group">
    <label><sup class="text-danger">*</sup> Название <small>({{ $lang }})</small></label>
    {!! Form::text('title_' . $lang, object_get($infoBlock, 'title:' . $lang), ['class' => 'form-control form-control-sm']) !!}
</div>

<div class="form-group">
    <label><sup class="text-danger">*</sup> Ссылка <small>({{ $lang }})</small></label>
    {!! Form::text('link_' . $lang, object_get($infoBlock, 'link:' . $lang), ['class' => 'form-control form-control-sm']) !!}
</div>

@if($lang == 'ua')
    <div class="row">
        <div class="form-group col-xs-2">
            <label>Позиция</label>
            {!! Form::text('position', object_get($infoBlock, 'position', $lastPosition), ['class' => 'form-control form-control-sm']) !!}
        </div>

        <div class="form-group col-xs-4">
            <label>Изображение <small>(360x350; jpeg, png)</small></label>

            <div class="input-group">
                <input type="file" name="image" class="file-upload-default">
                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                <span class="input-group-btn">
                    <button class="file-upload-browse btn btn-info" type="button">Выбрать файл</button>
                </span>
            </div>
        </div> 
    </div>
@endif

<div class="row">
    <div class="col-xs-8">
        <div class="form-group">
            <label><sup class="text-danger">*</sup> Описание <small>({{ $lang }})</small></label>
            {!! Form::textarea('description_' . $lang, object_get($infoBlock, 'description:' . $lang), ['class' => 'form-control form-control-sm']) !!}
        </div>
    </div>
    <div class="col-xs-4">
        @if(object_get($infoBlock, 'image'))
            <img src="/img/cache/f360x350/infoblock/{{ object_get($infoBlock, 'image') }}?t={{ time() }}" alt="" class="img-responsive">
        @endif
    </div>
</div>

<hr />

<h2>SEO</h2>

<div class="form-group">
    <label>Title <small>({{ $lang }})</small></label>
    {!! Form::text('seo_title_' . $lang, object_get($infoBlock, 'seo_title:' . $lang), ['class' => 'form-control form-control-sm']) !!}
</div>

<div class="form-group">
    <label>Description <small>({{ $lang }})</small></label>
    {!! Form::textarea('seo_description_' . $lang, object_get($infoBlock, 'seo_description:' . $lang), ['class' => 'form-control form-control-sm', 'rows' => '4'] ) !!}
</div>

<div class="form-group">
    <label>Keywords <small>({{ $lang }})</small></label>
    {!! Form::textarea('seo_keywords_' . $lang, object_get($infoBlock, 'seo_keywords:' . $lang), ['class' => 'form-control form-control-sm', 'rows' => '4'] ) !!}
</div>