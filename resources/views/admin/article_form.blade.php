@extends('adminlte::page')
@section('title', 'Статьи')
@section('content_header')
    @if (!empty($menu))
        <h1>Статьи - {{ object_get($menu, 'title') }}</h1>
    @else
        <h1>Статьи</h1>
    @endif
@stop
@section('content')

    {!! Form::model($article, ['url' => '/admin/group/' . $group_id . '/articles/save/' . object_get($article, 'id', 0), 'files' => true]) !!}

    <div class="box">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-xs-4">
                    <a href="/admin/group/{{ $group_id }}/articles" class="btn btn-primary"><i class="far fa-caret-square-left"></i></a>
                </div>

                <div class="col-xs-3">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#langUa" aria-controls="langUa" role="tab" data-toggle="tab">Укр</a>
                        </li>
                        <li role="presentation">
                            <a href="#langRu" aria-controls="langRu" role="tab" data-toggle="tab">Рус</a>
                        </li>
                        <li role="presentation">
                            <a href="#langEn" aria-controls="langEn" role="tab" data-toggle="tab">Eng</a>
                        </li>
                    </ul>
                </div>

                <div class="col-xs-5 text-right">
                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Сохранить</button>
                </div>
            </div>
        </div>

        <div class="box-body">

            @if ($group_id > 0)
                {!! Form::hidden('group_id', $group_id) !!}
            @else
                <div class="form-group">
                    <label>Меню</label>
                    {!! Form::select('group_id', $menuTree, object_get($article, 'group_id:ua'), ['class' => 'form-control form-control-sm']) !!}
                </div>
            @endif

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="langUa">
                    @include('admin.article_ext', ['lang' => 'ua'])
                </div>
                <div role="tabpanel" class="tab-pane" id="langRu">
                    @include('admin.article_ext', ['lang' => 'ru'])
                </div>
                <div role="tabpanel" class="tab-pane" id="langEn">
                    @include('admin.article_ext', ['lang' => 'en'])
                </div>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-block btn-success">Сохранить</button>

    {!! Form::close() !!}

@stop


