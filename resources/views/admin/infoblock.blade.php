@extends('adminlte::page')
@section('title', 'Инфо блок')
@section('content_header')
    <h1>Инфо блок</h1>
@stop
@section('content')

<div class="box">
    <div class="box-header with-border">

        <div class="row">
            <div class="col-xs-12 text-right">
                <a href="/admin/info-block/form/0" class="btn btn-primary"><i class="fas fa-plus"></i> Создать</a>
                @if($isArchive == 1)
                    <a href="/admin/info-block" class="btn btn-warning"><i class="fas fa-list-ul"></i> Список</a>
                @else
                    <a href="/admin/info-block/archive" class="btn btn-warning"><i class="fas fa-archive"></i> Архив</a>
                @endif
            </div>
        </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body">
        <table class="table table-md table-hover" id="table">
            <thead>
                <th>#</th>
                <th>Название</th>
                <th class="edit"></th>
            </thead>
        </table>
    </div>
    <!-- /.box-body -->
</div>

@stop

@section('js')

    <script>
        $(document).ready( function () {
            $('#table').DataTable({
                "language": {
                    "url": "/vendor/adminlte/plugins/dataTableRus.json"
                },
                serverSide: true,
                ajax: {
                    url: "/admin/info-block/ajax",
                    data: { isArchive: {{ $isArchive }} },
                    type: 'POST'
                },
                stateSave: true,
                "pageLength": 25,
                "aLengthMenu": [
                    [25, 50, 75, 100], [25, 50, 75, 100]
                ],
                "order": [
                    [ 0, "asc" ]
                ],
                "columns": [
                    { "data": "position" },
                    { "data": "title" },
                    { "data": "" },
                ],
                columnDefs: [
                    {
                        "targets": 0,
                        "orderable": true,
                        "searchable": false,
                        "width": "80px"
                    },
                    {
                        "targets": 1,
                        "orderable": true,
                        "searchable": true,
                        mRender: function ( data, type, row ) {
                            return '<a href="/admin/info-block/form/' + row.id + '">' + data + '</a>';
                        }
                    },
                    {
                        "targets": 2,
                        "orderable": false,
                        "searchable": false,
                        "width": "80px",
                        "className": 'text-right',
                        "render": function ( data, type, row ) {

                            var btnEdit = ''; 

                            if({{ $isArchive }})
                            {
                                btnEdit = '<a href="/admin/info-block/restore/' + row.id + '" class="btn btn-success btn-xs"><i class="fa fa-undo"></i></a>';
                            }
                            else
                            {
                                btnEdit = btnEdit + '<a href="/admin/info-block/destroy/' + row.id + '" class="btn btn-warning btn-xs btn-archive"><i class="fa fa-archive"></i></a>';
                            }

                            return btnEdit;
                        }
                    }
                ],
            });
        });
    </script>

@stop
