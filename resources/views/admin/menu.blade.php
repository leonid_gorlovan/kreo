@extends('adminlte::page')
@section('title', 'Меню')
@section('content_header')
    <h1>Меню</h1>

    @if ($subid > 0) 
        <ol class="breadcrumb">
            <li><a href="/admin/menu">Меню</a></li>
            <li>{{ object_get($menu, 'title') }}</li>
        </ol>
    @endif
@stop
@section('content')

<div class="box">
    <div class="box-header with-border">

        <div class="row">
            <div class="col-xs-6">
                @if ($subid > 0)
                    <a href="/admin/menu" class="btn btn-primary"><i class="far fa-caret-square-left"></i></a>
                @endif
            </div>
            <div class="col-xs-6 text-right">
                <a href="/admin/menu/{{ $subid }}/form/0" class="btn btn-primary"><i class="fas fa-plus"></i> Создать</a>
                @if($isArchive == 1)
                    <a href="/admin/menu/{{ $subid }}" class="btn btn-warning"><i class="fas fa-list-ul"></i> Список</a>
                @else
                    <a href="/admin/menu/{{ $subid }}/archive" class="btn btn-warning"><i class="fas fa-archive"></i> Архив</a>
                @endif
            </div>
        </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body">
        <table class="table table-md table-hover" id="table">
            <thead>
                <th>#</th>
                <th>Название</th>
                <th class="edit"></th>
            </thead>
        </table>
    </div>
    <!-- /.box-body -->
</div>

@stop

@section('js')

    <script>
        $(document).ready( function () {
            $('#table').DataTable({
                "language": {
                    "url": "/vendor/adminlte/plugins/dataTableRus.json"
                },
                serverSide: true,
                ajax: {
                    url: "/admin/menu/{{ $subid }}/ajax",
                    data: { isArchive: {{ $isArchive }} },
                    type: 'POST'
                },
                stateSave: true,
                "pageLength": 25,
                "aLengthMenu": [
                    [25, 50, 75, 100], [25, 50, 75, 100]
                ],
                "order": [
                    [ 0, "asc" ]
                ],
                "columns": [
                    { "data": "position" },
                    { "data": "title" },
                    { "data": "" },
                ],
                columnDefs: [
                    {
                        "targets": 0,
                        "orderable": true,
                        "searchable": false,
                        "width": "80px"
                    },
                    {
                        "targets": 1,
                        "orderable": true,
                        "searchable": true,
                        mRender: function ( data, type, row ) {
                            return '<a href="/admin/menu/{{ $subid }}/form/' + row.id + '">' + data + '</a>';
                        }
                    },
                    {
                        "targets": 2,
                        "orderable": false,
                        "searchable": false,
                        "width": "120px",
                        "className": 'text-right',
                        "render": function ( data, type, row ) {

                            var btnEdit = ''; 

                            if({{ $isArchive }})
                            {
                                btnEdit = '<a href="/admin/menu/{{ $subid }}/restore/' + row.id + '" class="btn btn-success btn-xs"><i class="fa fa-undo"></i></a>';
                            }
                            else
                            {
                                btnEdit = btnEdit +'<a href="/admin/group/' + row.id + '/articles" class="btn btn-primary btn-xs"><i class="far fa-file-alt"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                
                                btnEdit = btnEdit + '<a href="/admin/menu/' + row.id + '" class="btn btn-info btn-xs"><i class="fas fa-list"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                
                                btnEdit = btnEdit + '<a href="/admin/menu/{{ $subid }}/destroy/' + row.id + '" class="btn btn-warning btn-xs btn-archive"><i class="fa fa-archive"></i></a>';
                            }

                            return btnEdit;
                        }
                    }
                ],
            });
        });
    </script>

@stop
