<div class="form-group">
    <label><sup class="text-danger">*</sup> Название <small>({{ $lang }})</small></label>
    {!! Form::text('title_' . $lang, object_get($menu, 'title:' . $lang), ['class' => 'form-control form-control-sm']) !!}
</div>

@if($lang == 'ua')
    <div class="row">
        <div class="form-group col-xs-2">
            <label>Позиция</label>
            {!! Form::text('position', object_get($menu, 'position', $lastPosition), ['class' => 'form-control form-control-sm']) !!}
        </div>

        <div class="form-group col-xs-3">
            <label>Шаблон</label>
            {!! Form::select('tmpl', config('site.tmpl'), NULL, ['class' => 'form-control form-control-sm']) !!}
        </div>

        <div class="form-group col-xs-3">
            <label>Сортировка</label>
            {!! Form::select('order', config('site.orderContent'), NULL, ['class' => 'form-control form-control-sm']) !!}
        </div>  
    </div>
@endif

<hr />

<h2>SEO</h2>

<div class="form-group">
    <label>ЧПУ <small>({{ $lang }})</small></label>
    {!! Form::text('slug_' . $lang, object_get($menu, 'slug:' . $lang), ['class' => 'form-control form-control-sm']) !!}
</div>

<div class="form-group">
    <label>Title <small>({{ $lang }})</small></label>
    {!! Form::text('seo_title_' . $lang, object_get($menu, 'seo_title:' . $lang), ['class' => 'form-control form-control-sm']) !!}
</div>

<div class="form-group">
    <label>Description <small>({{ $lang }})</small></label>
    {!! Form::textarea('seo_description_' . $lang, object_get($menu, 'seo_description:' . $lang), ['class' => 'form-control form-control-sm', 'rows' => '4'] ) !!}
</div>

<div class="form-group">
    <label>Keywords <small>({{ $lang }})</small></label>
    {!! Form::textarea('seo_keywords_' . $lang, object_get($menu, 'seo_keywords:' . $lang), ['class' => 'form-control form-control-sm', 'rows' => '4'] ) !!}
</div>