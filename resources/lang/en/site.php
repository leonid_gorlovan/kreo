<?php

return [
    'title_site' => 'CREO PHARM IN UKRAINE',
    'see_details' => 'Read more',
    'we_offer' => 'We offer',

    'feedback' => 'Feedback',
    'message_send_feedback' => 'Your message has been sent.',
    'name' => 'Name',
    'message' => 'Your message',
    'submit' => 'Submit',
    'type_1' => 'Write to us',
    'type_2' => 'Leave a review',
    'type_3' => 'Report unwanted phenomena',

    'related_materials' => 'Related materials',
    'other_drugs' => 'Other drugs',
    'download_manual' => 'Download the manual',

    'high_quality_drugs' => 'High quality drugs',
    'high_quality_drugs_text' => 'Qualitative innovative products from European producers.',

    'сonfirmed_research' => 'Confirmed by research',
    'сonfirmed_research_text' => 'Efficacy and safety of each drug is confirmed by clinical studies.',

    'weighted_value' => 'Weighted value',
    'weighted_value_text' => 'Available drugs for each patient.',

    'actual_today' => 'Actual today',
    'actual_today_text' => 'The company represents drugs manufactured in Europe:',

    'actual_today_list1' => 'Pembina Blue',
    'actual_today_list2' => 'Valeo Dorm Duo',
    'actual_today_list3' => 'Valeo Dorm',
    'actual_today_list4' => 'Grindolis',
];